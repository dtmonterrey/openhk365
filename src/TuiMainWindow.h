/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TUIMAINWINDOW_H
#define TUIMAINWINDOW_H

#include <string>
#include "Hk365.h"
#include <thread>

class TuiMainWindow {
    public:
        TuiMainWindow(Hk365 *hk365);
        virtual ~TuiMainWindow();
    protected:
        static void threadUpdate(void *pthis);
        std::thread *updater;
    private:
        Hk365 *hk365;
        void initDigits();
        void drawFrame(int startrow, int startcol, int endrow, int endcol, bool clean);
        void drawDisplay(int mode, int range, int precision, bool clean);
        void drawValues(char value[5], int range, int precision);
        void drawValue(int value, int startRow, int startCol);
};

#endif // TUIMAINWINDOW_H
