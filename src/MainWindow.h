/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Hk365.h"
#include <thread>
#include <qwt_dial_needle.h>
#include <qwt_plot_curve.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Hk365 *hk365, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void update();
    void on_btnVDC_clicked();

    void on_btnVAC_clicked();

    void on_btnAAC_clicked();

    void on_btnADC_clicked();

private:
    Ui::MainWindow *ui;
    QwtDialSimpleNeedle *needle;
    Hk365 *hk365;
    QString title;
    double *x;
    double *y;
    int plotCacheSize;
    QwtPlotCurve *curve;
};

#endif // MAINWINDOW_H
