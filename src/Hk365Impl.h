/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HK365IMPL_H
#define HK365IMPL_H

#include "Hk365.h"

class Hk365Impl: public Hk365 {
    public:
        Hk365Impl(libusb_device *device);
        std::string toString();
        bool open();
        void close();
        hk365_data *read();
        bool changeMode(int mode, int range);
        virtual ~Hk365Impl();
    protected:
    private:
        int mode        = Hk365::MODE_V_DC;
        int range       = Hk365::RANGE_6;
        int precision   = Hk365::PRECISION_1000;
};

#endif // HK365IMPL_H
