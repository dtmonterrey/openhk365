/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "UsbManager.h"

UsbManager::UsbManager() {
    std::cout << "Initiating... ";
    libusb_init(NULL);
    libusb_set_debug(NULL, LIBUSB_LOG_LEVEL_WARNING);
    cnt = libusb_get_device_list(NULL, &devs);
    std::cout << "done.\n";
}

void UsbManager::printDevices() {
    libusb_device *dev;
    int i = 0, j = 0;
    uint8_t path[8];
    while ((dev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0) {
            fprintf(stderr, "failed to get device descriptor");
            return;
        }
        printf("%04x:%04x (bus %d, device %d)",
        desc.idVendor, desc.idProduct, libusb_get_bus_number(dev), libusb_get_device_address(dev));
        r = libusb_get_port_numbers(dev, path, sizeof(path));
        if (r > 0) {
            printf(" path: %d", path[0]);
            for (j = 1; j < r; j++)
                printf(".%d", path[j]);
        }
        printf("\n");
    }
}

Hk365 *UsbManager::getDevice() {
    libusb_device *dev;
    int i = 0;
    while ((dev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0) {
            fprintf(stderr, "failed to get device descriptor");
            Hk365 *hk365 = new Hk365Fake(nullptr);
            return hk365;
        }
        if (desc.idVendor == Hk365::VENDOR_ID && desc.idProduct == Hk365::DEVICE_ID) {
            Hk365 *hk365 = new Hk365Impl(dev);
            return hk365;
        }
    }
    Hk365 *hk365 = new Hk365Fake(nullptr);
    return hk365;
}

UsbManager::~UsbManager() {
    std::cout << "Closing... ";
    if (devs != NULL) {
        libusb_free_device_list(devs, 1);
    }
    libusb_exit(NULL);
    std::cout << "done.\n";
}
