/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USBMANAGER_H
#define USBMANAGER_H

#include "libusb-1.0/libusb.h"
#include <stdio.h>
#include <iostream>
#include "Hk365.h"
#include "Hk365Impl.h"
#include "Hk365Fake.h"

class UsbManager {
    public:
        UsbManager();
        void printDevices();
        Hk365 *getDevice();
        virtual ~UsbManager();
    protected:
    private:
        libusb_device **devs;
        int r;
        ssize_t cnt;
};

#endif // USBMANAGER_H
