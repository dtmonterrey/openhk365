/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QFile>
#include <QMessageBox>
#include <QFontDatabase>
#include <string>
#include <sstream>
#include "UsbManager.h"
#include "TuiMainWindow.h"
#include "MainWindow.h"

void initFonts() {
    QStringList list;
    list << "resources/Nova_Mono/NovaMono.ttf";
    int fontID(-1);
    bool fontWarningShown(false);
    for (QStringList::const_iterator constIterator = list.constBegin(); constIterator != list.constEnd(); ++constIterator) {
        QFile res(":/fonts/" + *constIterator);
        if (res.open(QIODevice::ReadOnly) == false) {
            if (fontWarningShown == false) {
                QMessageBox::warning(0, "Application", (QString)"Can't open font 'NovaMono'.");
                fontWarningShown = true;
            }
        } else {
            fontID = QFontDatabase::addApplicationFontFromData(res.readAll());
            if (fontID == -1 && fontWarningShown == false) {
                QMessageBox::warning(0, "Application", (QString)"Can't open font 'NovaMono'.");
                fontWarningShown = true;
            }
        }
    }
}

int main(int argc, char *argv[]) {
    UsbManager usbManager;

    // get device
    Hk365 *hk365 = usbManager.getDevice();
    if (hk365 == nullptr) {
        std::cout << "Device not found!!!\n";
        return -1;
    }
    std::cout << "Hantek device: ";
    std::cout << hk365->toString() << "\n";

    // open device
    std::cout << "Opening device... ";
    if (hk365->open()) {
        std::cout << "OK\n";
    } else {
        std::cout << "error\n";
        return -1;
    }

    bool tui = false;
    if (tui) {
        // starts CURSES mode
        TuiMainWindow mainWindow(hk365);
        usbManager.~UsbManager();
        exit(0);
    } else {
        QApplication a(argc, argv);
        initFonts();
        MainWindow w(hk365, 0);
        w.show();
        return a.exec();
    }

}
