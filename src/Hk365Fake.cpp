/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Hk365Fake.h"
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

Hk365Fake::Hk365Fake(libusb_device *device) {
    if (device != nullptr) {
        this->device = device;
    } else {
        this->device = nullptr;
    }

}

std::string Hk365Fake::toString() {
    std::stringstream returnString;

    returnString << "FAKE Hantek 365A on bus -1, device -1";
    return returnString.str();
}

bool Hk365Fake::open() {
    return true;
}

Hk365::hk365_data *Hk365Fake::read() {
    Hk365::hk365_data *data = new Hk365::hk365_data;
    srand(time(NULL));
    data->mode      = this->mode;
    data->range     = this->range;
    data->precision = this->precision;
    data->value[0] = ((rand() % 10) < 5 ? '-' : ' ');
    data->value[1] = 48 + (rand() % 6);
    data->value[2] = 48 + (rand() % 10);
    data->value[3] = 48 + (rand() % 10);
    data->value[4] = 48 + (rand() % 10);
    data->value[5] = '\0';
    return data;
}

bool Hk365Fake::changeMode(int mode, int range) {
    this->mode = mode;
    this->range = range;
    return true;
}

void Hk365Fake::close() {
    if (this->handle != nullptr) {
        libusb_close(this->handle);
        this->handle = nullptr;
    }
}

Hk365Fake::~Hk365Fake() {
    //dtor
}
