/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <iostream>
#include <thread>
#include <chrono>
#include <qtimer.h>

MainWindow::MainWindow(Hk365 *hk365, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->hk365 = hk365;
    ui->setupUi(this);

    // title
    title = "OpenHk365 - device: ";
    title.append(this->hk365->toString().data());
    this->setWindowTitle(title);

    // Dial
    this->needle = new QwtDialSimpleNeedle(
        QwtDialSimpleNeedle::Arrow, true, Qt::red,
        QColor( Qt::gray ).light( 130 ) );
    this->ui->Dial->setNeedle(needle);

    // Plot
    this->plotCacheSize = 255;
    this->curve = new QwtPlotCurve("Curve 1");
    x = new double[this->plotCacheSize];
    y = new double[this->plotCacheSize];
    for (int i=0; i<this->plotCacheSize; i++) {
        x[i] = i;
        y[i] = 0;
    }
    this->curve->setRawSamples(x,y,this->plotCacheSize);
    this->curve->attach(this->ui->qwtPlot);
    this->curve->setPen(QColor(Qt::green), 1.0);
    this->ui->qwtPlot->setAxisScale(QwtPlot::xBottom, 0.0, this->plotCacheSize);
    this->ui->qwtPlot->enableAxis(QwtPlot::xBottom, false);
    this->ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -1.0, 1.0);
    this->curve->show();

    // Timer
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);

}

void MainWindow::update() {
    Hk365::hk365_data *data = this->hk365->read();

    std::cout << this->hk365->dataToString(data) << std::endl;

    // update mode and range
    switch (data->mode) {
    case Hk365::MODE_mV_AC:
        switch(data->range) {
        case Hk365::RANGE_60:
            this->ui->labelCurrentMode->setText("60 mV AC");
            break;
        case Hk365::RANGE_600:
            this->ui->labelCurrentMode->setText("600 mV AC");
            break;
        }
        break;
    case Hk365::MODE_V_AC:
        switch(data->range) {
        case Hk365::RANGE_6:
            this->ui->labelCurrentMode->setText("6 V AC");
            break;
        case Hk365::RANGE_60:
            this->ui->labelCurrentMode->setText("60 V AC");
            break;
        case Hk365::RANGE_600:
            this->ui->labelCurrentMode->setText("600 V AC");
            break;
        }
        break;
    case Hk365::MODE_mV_DC:
        switch(data->range) {
        case Hk365::RANGE_60:
            this->ui->labelCurrentMode->setText("60 mV DC");
            break;
        case Hk365::RANGE_600:
            this->ui->labelCurrentMode->setText("600 mV DC");
            break;
        }
        break;
    case Hk365::MODE_V_DC:
        switch(data->range) {
        case Hk365::RANGE_6:
            this->ui->labelCurrentMode->setText("6 V DC");
            break;
        case Hk365::RANGE_60:
            this->ui->labelCurrentMode->setText("60 V DC");
            break;
        case Hk365::RANGE_600:
            this->ui->labelCurrentMode->setText("600 V DC");
            break;
        case Hk365::RANGE_800:
            this->ui->labelCurrentMode->setText("800 V DC");
            break;
        }
        break;
    case Hk365::MODE_A_AC:
    case Hk365::MODE_mA_AC:
        break;
    case Hk365::MODE_A_DC:
    case Hk365::MODE_mA_DC:
        break;
    case Hk365::MODE_OHM:
    case Hk365::MODE_OHM_K:
    case Hk365::MODE_OHM_M:
        break;
    default:
        return;
    }

    QString value = "";
    for (int i=0; i<5; i++) {
        if (i == 0) {
            if (data->value[0] == '-') {
                value.append('-');
            } else {
                value.append(' ');
            }
        } else {
            value.append(data->value[i]);
            if ((3-(data->precision)) == i - 1) {
                value.append('.');
            }
        }
    }

    // lcd display value
    this->ui->lcdNumber->display(value);
    // dial range and value
    this->ui->Dial->setUpperBound(double(data->range));
    double dValue = value.toDouble();
    double absValue = dValue;
    if (dValue < 0) {
        absValue = dValue < 0 ? (-1 * dValue) : dValue;
        this->needle = new QwtDialSimpleNeedle(
                    QwtDialSimpleNeedle::Arrow, true, Qt::blue,
                    QColor( Qt::gray ).light( 130 ) );
    } else {
        this->needle = new QwtDialSimpleNeedle(
                    QwtDialSimpleNeedle::Arrow, true, Qt::red,
                    QColor( Qt::gray ).light( 130 ) );
    }
    this->ui->Dial->setNeedle(needle);
    this->ui->Dial->setValue(absValue);
    // plot
    for (int i=0; i<this->plotCacheSize; i++) {
        x[i] = i;
        if (i>0) y[i-1] = y[i];
        switch (data->mode) {
        case Hk365::MODE_mA_AC:
        case Hk365::MODE_mA_DC:
        case Hk365::MODE_mV_AC:
        case Hk365::MODE_mV_DC:
            if (data->range < 10) {
                y[i] = dValue / 10.0;
            } else if (data->range < 100) {
                y[i] = dValue / 100.0;
            } else if (data->range < 1000) {
                y[i] = dValue / 1000.0;
            }
            break;
        default:
            y[i] = dValue;
            break;
        }
    }
    this->curve->setRawSamples(x,y,this->plotCacheSize);
    this->curve->attach(this->ui->qwtPlot);
    //this->curve->setPen(QColor(Qt::green), 1.0);
    float range = data->range;
    switch (data->mode) {
    case Hk365::MODE_mA_AC:
    case Hk365::MODE_mA_DC:
    case Hk365::MODE_mV_AC:
    case Hk365::MODE_mV_DC:
        if (data->range < 10) {
            range = data->range / 10.0;
        } else if (data->range < 100) {
            range = data->range / 100.0;
        } else if (data->range < 1000) {
            range = data->range / 1000.0;
        }
    }

    this->ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -1.0 * range, range);
    this->curve->show();
}

MainWindow::~MainWindow()
{
    this->hk365->close();
    delete ui;
}

void MainWindow::on_btnVDC_clicked()
{
    this->hk365->changeMode(Hk365::MODE_V_DC_AUTO, NULL);
}

void MainWindow::on_btnVAC_clicked()
{
    this->hk365->changeMode(Hk365::MODE_V_AC_AUTO, NULL);
}

void MainWindow::on_btnAAC_clicked()
{
    this->hk365->changeMode(Hk365::MODE_AAC_AUTO, NULL);
}

void MainWindow::on_btnADC_clicked()
{
    this->hk365->changeMode(Hk365::MODE_ADC_AUTO, NULL);
}
