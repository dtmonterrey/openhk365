/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TuiMainWindow.h"
#include "ncursesw/ncurses.h"
#include <iostream>
//#include <thread>
//#include <chrono>

const char CTRL_B = 2;    // Define names for some control characters.
const char CTRL_R = 18;
const char CTRL_U = 21;
const char CTRL_X = 24;
const char CTRL_N = 14;
std::string digits[13][5];

bool curses_started = false;
Hk365 *hk365;

TuiMainWindow::TuiMainWindow(Hk365 *hk365) {
    this->hk365 = hk365;
    // initialize digits
    initDigits();

    // starts curses mode
    if (curses_started) {
        refresh();
    } else {
        initscr();
        cbreak();
        noecho();
        intrflush(stdscr, false);
        keypad(stdscr, true);

        // main window
        drawFrame(0, 0, LINES-1, COLS-1, false);
        // digital display window
        drawDisplay(Hk365::MODE_mV_DC, Hk365::RANGE_6, Hk365::PRECISION_1000, false);
        // read first value
        drawValues(hk365->read()->value, Hk365::RANGE_6, Hk365::PRECISION_1000);

        // start a thread that updates the reading

        updater = new std::thread(TuiMainWindow::threadUpdate, this);

        // write instructions
        mvaddstr(10, 2, "CTRL+X exit | CTRL+R refresh ");

        //move(11, 2); // Move cursor to center of window.
        //addch(' ' | A_REVERSE);
        //addch(' ');

        int row, col;  // The current row and column of the cursor.
        bool done = false;
        while (!done) {
            getyx(stdscr, row, col);
            int ch = getch();
            switch (ch){
            case CTRL_R:
                drawValues(hk365->read()->value, Hk365::RANGE_6, Hk365::PRECISION_1000);
                break;
            case CTRL_X:   // End the loop if user types CONTROL-X
                done = true;
                break;
            case '\n':     // User pressed RETURN.
                if (row < LINES-2) {
                    move(row+1,1);   // Move to beginning of next line.
                } else {
                    move (row,1);    // On last line.  Move to start of the line.
                }
                break;
            case KEY_UP:    // Move cursor up, unless already at top of window.
                /*if (row > 1) {
                    move(row-1,col);
                }*/
                break;
            case KEY_DOWN:  // Move cursor down, unless already at bottom.
                /*if (row < LINES-2)
                    move(row+1,col);*/
                break;
            case KEY_LEFT:  // Move cursor left, unless already at left edge.
                /*if (col > 1)
                    move(row,col-1);*/
                break;
            case KEY_RIGHT: // Move cursor right, unless already at right edge.
                /*if (col < COLS-2)
                    move(row,col+1);*/
                break;
            case KEY_BACKSPACE:  // User pressed the backspace key.
                if (col > 1) {
                    // Cursor is not in the first column.
                    // Overwrite previous char with a space.
                    //move(row,col-1);
                    //addch(' ');
                    //move(row,col-1);
                } else if (row > 1) {
                    // Cursor was in the first column, but not on the
                    // first row.  Move to end of previous row and
                    // overwrite the last char on that row.
                    //move(row-1,COLS-2);
                    //addch(' ');
                    //move(row-1,COLS-2);
                }
                break;
            default:
                if (ch >= ' ' && ch <= 126) {
                    // The user typed a printable character.
                    // Put it at the current cursor position.  If that's
                    // the end of the row, move cursor to the next row
                    // (or, if already  on the last row, to the beginning
                    // of the row.)
                    /*addch(ch);
                    if (col == COLS-2) {
                        if (row < LINES-2) {
                            move(row+1,1);
                        } else {
                            move(row,col);
                        }
                    }*/
                }
                break;
            } // end switch
            refresh();  // Make sure any changes made become visible on screen.
        } // end while
        curses_started = true;
    }
    // The End!
    if (curses_started && !isendwin()) {
        endwin();
    }
}

void TuiMainWindow::threadUpdate(void *pthis) {
    while (1) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        TuiMainWindow *pt = (TuiMainWindow *) pthis;
        pt->drawValues(pt->hk365->read()->value, Hk365::RANGE_6, Hk365::PRECISION_1000);
    }
}

/*
 * This routine draws a frame around a specified part of the console
 * screen, using special characters such as ACS_ULCORNER and ACS_VLINE.
 * The frame extends from startrow to endrow vertically and from
 * startcol to endcol horizontally.  (Rows and columns are numbered
 * starting from 0.)  Note that the interior of the frame extends
 * from startrow+1 to endrow-1 vertically and from startcol+1 to
 * endcol-1 horizontally.
 */
void TuiMainWindow::drawFrame(int startrow, int startcol, int endrow, int endcol, bool clean) {
    // save current cursor position
    int saverow, savecol;
    getyx(stdscr, saverow, savecol);

    // top line
    mvaddch(startrow, startcol, ACS_ULCORNER);
    for (int i=startcol+1; i < endcol; i++) {
        addch(ACS_HLINE);
    }
    addch(ACS_URCORNER);

    // left and right lines
    for (int i=startrow+1; i < endrow; i++) {
        mvaddch(i,startcol,ACS_VLINE);
        mvaddch(i,endcol,ACS_VLINE);
    }

    // bottom line
    mvaddch(endrow,startcol,ACS_LLCORNER);
    for (int i=startcol + 1; i < endcol; i++) {
        addch(ACS_HLINE);
    }
    addch(ACS_LRCORNER);

    // clean area?
    if (clean) {
        for (int i=startrow+1; i<endrow-1; i++) {
            for (int j=startcol+1; j<endcol-1; j++) {
                mvaddch(i, j, ' ');
            }
        }
    }

    // restore cursor position
    move(saverow,savecol);
    refresh();
}

void TuiMainWindow::drawDisplay(int mode, int range, int precision, bool clean) {
    // draw display cage
    drawFrame(1, 1, 9, 31, clean);
    // calculate start range
    std::string range_start = "0";
    for (int i=0; i<precision; i++) {
        if (i==0) range_start += ",";
        range_start += "0";
    }
    // calculate end range
    std::string range_end = std::to_string(range);
    for (int i=0; i<precision; i++) {
        if (i==0) range_end += ",";
        range_end += "0";
    }

    // mode
    std::string strMode;
    switch (mode) {
    case Hk365::MODE_A_AC:
        strMode = "A AC";
        break;
    case Hk365::MODE_A_DC:
        strMode = "A DC";
        break;
    case Hk365::MODE_mA_AC:
        strMode = "mA AC";
        break;
    case Hk365::MODE_mA_DC:
        strMode = "mA DC";
        break;
    case Hk365::MODE_mV_AC:
        strMode = "mV AC";
        break;
    case Hk365::MODE_mV_DC:
        strMode = "mV DC";
        break;
    case Hk365::MODE_uF:
        strMode = "uF";
        break;
    case Hk365::MODE_nF:
        strMode = "nF";
        break;
    case Hk365::MODE_OHM:
        strMode = "Ohm";
        break;
    case Hk365::MODE_OHM_K:
        strMode = "Ohm K";
        break;
    case Hk365::MODE_OHM_M:
        strMode = "Ohm M";
        break;
    case Hk365::MODE_V_AC:
        strMode = "V AC";
        break;
    case Hk365::MODE_V_DC:
        strMode = "V DC";
        break;
    }
    attron(A_BOLD);
    std::string strRange = range_start + " ~ " + range_end;
    move(2, 3);
    addstr(strRange.c_str());
    addch(' ');
    attron(A_REVERSE);
    addstr(strMode.c_str());
    attroff(A_REVERSE); attroff(A_BOLD);
}

void TuiMainWindow::drawValues(char value[5], int range, int precision) {
    // save current cursor position
    int saverow, savecol;
    getyx(stdscr, saverow, savecol);
    // draw digits
    for (int i=0; i<5; i++) {
        if (i == 0) {
            if (value[0] == '-') {
                drawValue(11, 4, 3);
            } else {
                drawValue(12, 4, 3);
            }
        } else {
            drawValue(value[i]-'0', 4, 1+(i*6));
            if ((3-precision) == i - 1) {
                drawValue(10, 4, 4+(i*6));
            }
        }
    }
    // restore cursor position
    move(saverow,savecol);
    refresh();
}

void TuiMainWindow::drawValue(int value, int startRow, int startCol) {
    for (int i=0; i<5; i++) {
        for (int j=0; j<3; j++) {
            char c = digits[value][i][j];
            if (c != ' ') {
                mvaddch(startRow+i, startCol+j, ' ' | A_REVERSE);
            } else {
                mvaddch(startRow+i, startCol+j, ' ');
            }
        }
    }
}

void TuiMainWindow::initDigits() {
    digits[0][0] = "###";
    digits[0][1] = "# #";
    digits[0][2] = "# #";
    digits[0][3] = "# #";
    digits[0][4] = "###";

    digits[1][0] = " # ";
    digits[1][1] = "## ";
    digits[1][2] = " # ";
    digits[1][3] = " # ";
    digits[1][4] = "###";

    digits[2][0] = "###";
    digits[2][1] = "  #";
    digits[2][2] = " # ";
    digits[2][3] = "#  ";
    digits[2][4] = "###";

    digits[3][0] = "###";
    digits[3][1] = "  #";
    digits[3][2] = " ##";
    digits[3][3] = "  #";
    digits[3][4] = "###";

    digits[4][0] = " ##";
    digits[4][1] = "# #";
    digits[4][2] = "###";
    digits[4][3] = "  #";
    digits[4][4] = "  #";

    digits[5][0] = "###";
    digits[5][1] = "#  ";
    digits[5][2] = "###";
    digits[5][3] = "  #";
    digits[5][4] = "## ";

    digits[6][0] = "  #";
    digits[6][1] = " # ";
    digits[6][2] = "###";
    digits[6][3] = "# #";
    digits[6][4] = "###";

    digits[7][0] = "###";
    digits[7][1] = "  #";
    digits[7][2] = " # ";
    digits[7][3] = "#  ";
    digits[7][4] = "#  ";

    digits[8][0] = "###";
    digits[8][1] = "# #";
    digits[8][2] = "###";
    digits[8][3] = "# #";
    digits[8][4] = "###";

    digits[9][0] = "###";
    digits[9][1] = "# #";
    digits[9][2] = "###";
    digits[9][3] = "  #";
    digits[9][4] = "  #";

    digits[10][0] = "   ";
    digits[10][1] = "   ";
    digits[10][2] = "   ";
    digits[10][3] = "   ";
    digits[10][4] = " # ";

    digits[11][0] = "   ";
    digits[11][1] = "   ";
    digits[11][2] = "###";
    digits[11][3] = "   ";
    digits[11][4] = "   ";

    digits[12][0] = "   ";
    digits[12][1] = "   ";
    digits[12][2] = "   ";
    digits[12][3] = "   ";
    digits[12][4] = "   ";
}

TuiMainWindow::~TuiMainWindow() {
    if (curses_started && !isendwin()) {
        endwin();
    }
    //pthread_exit(this->updater);
}




