/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Hk365Impl.h"
#include <sstream>
#include <iostream>

Hk365Impl::Hk365Impl(libusb_device *device) {
    if (device != nullptr) {
        this->device = device;
    } else {
        this->device = nullptr;
    }

}

std::string Hk365Impl::toString() {
    std::stringstream returnString;

    returnString << "Hantek 365A on bus "
        << int(libusb_get_bus_number(this->device))
        << ", device "
        << int(libusb_get_device_address(this->device));
    return returnString.str();
}

bool Hk365Impl::open() {
    if (this->device != nullptr) {
        int err = libusb_open(this->device, &this->handle);
        if (err == 0) {
            return true;
        }
    }
    return false;
}

Hk365::hk365_data *Hk365Impl::read() {
    int lengthDataTransmitted = -1;
    Hk365::hk365_data *data = new Hk365::hk365_data;
    // send command
    unsigned char dataOut[2] = {0x1, 0xf};
    libusb_bulk_transfer(this->handle, 0x02, &dataOut[0], 2, &lengthDataTransmitted, 0);
    // receive data
    unsigned char dataIn[15];
    int r = libusb_bulk_transfer(this->handle, 0x81, &dataIn[0], 15, &lengthDataTransmitted, 0);
    if (r > 0) {
        data->mode = this->mode;
        data->range = this->range;
        data->precision = this->precision;
        data->value[0] = '-';
        data->value[1] = '0';
        data->value[2] = '0';
        data->value[3] = '0';
        data->value[4] = '0';
        data->value[5] = '\0';
    } else {
        for (int i=0; i<15; i++) {
            std::cout << std::hex << "0x" << (int)dataIn[i] << ' ';
        }
        std::cout << std::endl;
        // check mode
        switch (dataIn[11]) {
        case 0x80:
            // Volts mode
            switch (dataIn[8]) {
            case 0x29:
                if (dataIn[10] == 0x40) {
                    data->mode = Hk365::MODE_mV_AC;
                } else {
                    data->mode = Hk365::MODE_V_AC;
                }
                break;
            case 0x31:
                if (dataIn[10] == 0x40) {
                    data->mode = Hk365::MODE_mV_DC;
                } else {
                    data->mode = Hk365::MODE_V_DC;
                }
                break;
            default:
                if (dataIn[10] == 0x40) {
                    data->mode = Hk365::MODE_mV_DC;
                } else {
                    data->mode = Hk365::MODE_V_DC;
                }
                break;
            }
            break;
        case 0x40:
            // Ampere mode
            break;
        case 0x20:
            // Ohm mode
            break;
        }
        // check range
        switch (dataIn[7]) {
        case 0x32:
            data->range = Hk365::RANGE_60;
            data->precision = Hk365::PRECISION_100;
            break;
        case 0x34:
            data->range = Hk365::RANGE_600;
            data->precision = Hk365::PRECISION_10;
            break;
        case 0x31:
            data->range = Hk365::RANGE_6;
            data->precision = Hk365::PRECISION_1000;
            break;
        case 0x30:
            data->range = Hk365::RANGE_800;
            data->precision = Hk365::PRECISION_1;
            break;
        default:
            data->range = Hk365::RANGE_6;
            data->precision = Hk365::PRECISION_1000;
            break;
        }
        // set value
        data->value[0] = dataIn[1];
        data->value[1] = dataIn[2];
        data->value[2] = dataIn[3];
        data->value[3] = dataIn[4];
        data->value[4] = dataIn[5];
        data->value[5] = '\0';
    }
    return data;
}

bool Hk365Impl::changeMode(int mode, int range) {
    this->mode = mode;
    this->range = range;

    // TODO implement request to switch mode
    unsigned char dataOut[2] = {0x03, 0xa4};
    switch (mode) {
    case Hk365::MODE_V_DC_AUTO:
        dataOut[1] = 0xa0;  // DC auto mode
        break;
    case Hk365::MODE_V_AC_AUTO:
        dataOut[1] = 0xb0;  // AC auto mode
        break;
    case Hk365::MODE_mV_DC:
        switch (range) {
        case Hk365::RANGE_60:
            dataOut[1] = 0xa1;
            break;
        case Hk365::RANGE_600:
            dataOut[1] = 0xa2;
            break;
        default:
            dataOut[1] = 0xa0;  // DC auto mode
            break;
        }
        break;
    case Hk365::MODE_V_DC:
        switch (range) {
        case Hk365::RANGE_6:
            dataOut[1] = 0xa3;
            break;
        case Hk365::RANGE_60:
            dataOut[1] = 0xa4;
            break;
        case Hk365::RANGE_600:
            dataOut[1] = 0xa5;
            break;
        case Hk365::RANGE_800:
            dataOut[1] = 0xa6;
            break;
        default:
            dataOut[1] = 0xa0;  // DC auto mode
            break;
        }
        break;
    /* not implemented, need to know how to interpret the data received
    case Hk365::MODE_V_AC:
    case Hk365::MODE_mV_AC:
        dataOut[1] = 0xb0;
        break;
    case Hk365::MODE_A_DC:
    case Hk365::MODE_mA_DC:
        dataOut[1] = 0xc0;
        break;
    case Hk365::MODE_A_AC:
    case Hk365::MODE_mA_AC:
        dataOut[1] = 0xd0;
        break;
    case Hk365::MODE_OHM:
    case Hk365::MODE_OHM_K:
    case Hk365::MODE_OHM_M:
        dataOut[1] = 0xe0;
        break;
    case Hk365::MODE_uF:
    case Hk365::MODE_nF:
        dataOut[1] = 0xf1;
        break;
    */
    default:
        dataOut[1] = 0xa0;
        break;
    }

    int lengthDataTransmitted = -1;
    unsigned char dataIn[15];
    do {
        // send command
        libusb_bulk_transfer(this->handle, 0x02, dataOut, 2, &lengthDataTransmitted, 0);
        // receive data
        int r = libusb_bulk_transfer(this->handle, 0x81, dataIn, 15, &lengthDataTransmitted, 0);
        if (r > 0) {
            std::cout << "ERROR" << std::endl;
            return false;   // ERROR
        }
    } while (dataIn[0] != 0xdd);
    return true;
}

void Hk365Impl::close() {
    if (this->handle != nullptr) {
        libusb_close(this->handle);
        this->handle = nullptr;
    }
}

Hk365Impl::~Hk365Impl()
{
    //dtor
}
