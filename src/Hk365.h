/*
 * Desktop Qt based application for Hantek 365A
 * Copyright (C) 2014  Evandro Pires Alves
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HK365_H
#define HK365_H

#include <string>
#include "libusb-1.0/libusb.h"

class Hk365 {
    public:
        const static uint16_t VENDOR_ID = 0x0483;
        const static uint16_t DEVICE_ID = 0x5722;
        // V AC
        const static int MODE_V_AC_AUTO = 10;   // AUTO
        const static int MODE_V_AC      = 11;   // V AC
        const static int MODE_mV_AC     = 12;   // mV AC
        // V DC
        const static int MODE_V_DC_AUTO = 20;   // AUTO
        const static int MODE_V_DC      = 21;   // V DC
        const static int MODE_mV_DC     = 22;   // mV DC
        // A AC
        const static int MODE_AAC_AUTO  = 30;   // AUTO
        const static int MODE_A_AC      = 31;   // A AC
        const static int MODE_mA_AC     = 32;   // mA AC
        // A DC
        const static int MODE_ADC_AUTO  = 40;   // AUTO
        const static int MODE_A_DC      = 41;   // A DC
        const static int MODE_mA_DC     = 42;   // mA DA
        // Ohm
        const static int MODE_OHM_AUTO  = 50;   // AUTO
        const static int MODE_OHM       = 51;   // Ohm
        const static int MODE_OHM_K     = 52;   // Ohm x 1 000
        const static int MODE_OHM_M     = 53;   // Ohm x 1 000 000

        const static int MODE_uF   = 6;    // μF
        const static int MODE_nF   = 61;   // nF
        const static int RANGE_4   = 4;    // 0 ~ 4,000
        const static int RANGE_6   = 6;    // 0 ~ 6,000
        const static int RANGE_10  = 10;   // 0 ~ 10,00
        const static int RANGE_40  = 40;   // 0 ~ 40,00
        const static int RANGE_60  = 60;   // 0 ~ 60,00
        const static int RANGE_400 = 400;  // 0 ~ 400,0
        const static int RANGE_600 = 600;  // 0 ~ 600,0
        const static int RANGE_800 = 800;  // 0 ~ 800,0
        const static int PRECISION_1   = 0;// 1
        const static int PRECISION_10  = 1;// 0,1
        const static int PRECISION_100 = 2;// 0,01
        const static int PRECISION_1000= 3;// 0,001
        struct hk365_data {
            int mode;
            int range;
            int precision;
            char value[6];
        };
        Hk365();
        virtual std::string toString() =0;
        virtual bool open() =0;
        virtual void close() =0;
        virtual hk365_data *read() =0;
        virtual bool changeMode(int mode, int range) =0;
        std::string dataToString(hk365_data *data);
        virtual ~Hk365();
    protected:
        libusb_device *device = nullptr;
        libusb_device_handle *handle = nullptr;
    private:
};

#endif // HK365_H
