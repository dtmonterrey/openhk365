#-------------------------------------------------
#
# Project created by QtCreator 2014-10-29T11:25:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = openhk365
TEMPLATE = app


SOURCES += src/main.cpp\
    src/MainWindow.cpp \
    src/Hk365.cpp \
    src/Hk365Fake.cpp \
    src/Hk365Impl.cpp \
    src/TuiMainWindow.cpp \
    src/UsbManager.cpp

HEADERS += src/Hk365.h \
    src/Hk365Fake.h \
    src/Hk365Impl.h \
    src/MainWindow.h \
    src/TuiMainWindow.h \
    src/UsbManager.h

LIBS += -lusb-1.0\
    -lcurses\
    -lqwt

FORMS   += src/MainWindow.ui

CONFIG  += c++11

# Quoted include directories
#INCLUDEPATH_QUOTE = "$${IN_PWD}/src"

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    LICENSE.txt

unix:!macx: LIBS += -L/usr/local/qwt-6.1.2/lib/ -lqwt

INCLUDEPATH += /usr/local/qwt-6.1.2/include
DEPENDPATH += /usr/local/qwt-6.1.2/include
