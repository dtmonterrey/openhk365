For Future Reference
====================

Protocol reverse engineering
----------------------------

Work:

1. send control
2. receive blank
3. send blank
4. 1. if control changed, receive 0xDD
4. 2. else receive aknowladge 0xAA
5. send control
6. receive data 0xAA ...
7. goto to 1.

### Control frame sendt
* 01 0F --> keep last config
* V DC
* * 03 A0 --> AUTO
* * 03 A1 --> 60,00mV
* * 03 A2 --> 600,0mV
* * 03 A3 --> 6,000 V
* * 03 A4 --> 60,00 V
* * 03 A5 --> 600,0 V
* * 03 A6 --> 800,0 V
* V AC
* * 03 B0 --> AUTO
* * 03 B1 --> 60,00mV
* * 03 B2 --> 600,0mV
* * 03 B3 --> 6,000V
* * 03 B4 --> 60,00V
* * 03 B5 --> 600,0V
* Ohm DC
* * 03 E0 --> AUTO
* * 03 E1 --> 60,00MOhm ???
* * 03 E1 --> 600,0Ohm ???
* * 03 E2 --> 6,000kOhm
* * 03 E3 --> 60,00kOhm
* * 03 E4 --> 600,0kOhm
* * 03 E5 --> 6,000MOhm
* * 03 E6 --> 60,00MOhm
* A DC
* * 03 C0 --> AUTO
* * 03 C1 --> 60,00mA
* * 03 C2 --> 600,0mA
* * 03 C3 --> 10,00A
* A AC
* * 03 D0 --> AUTO
* * 03 D1 --> 60,00mA
* * 03 D2 --> 600,0mA
* * 03 D3 --> 10,00A
* 03 F0 --> Diode
* 03 F2 --> Conti
* 03 F1 --> Capacitor
* 03 F5 --> Temperature (no available?)

### Control frame received
* AA  --> config no change
* DD --> config changed

### Data frame

```
#!txt

A0 ## ## ## ## ## ## ##    ## ## ## ## ## ## ##
^  ^  ^  ^  ^  ^  ^  ^     ^  ^  ^  ^  ^  ^  ^
|  |  |  |  |  |  |  |     |  |  |  |  |  |  \ always 0xA0 delimitator ???
|  |  |  |  |  |  |  |     |  |  |  |  |  \--- ???
|  |  |  |  |  |  |  |     |  |  |  |  \------ ???
|  |  |  |  |  |  |  |     |  |  |  \--------- unit: V:0x80 A:0x40 Ohm:0x20
|  |  |  |  |  |  |  |     |  |  \------------ multiplicator mili:0x40 normal:0x0 mega:0x10 k:0x20
|  |  |  |  |  |  |  |     |  \--------------- ???always 0x4???
|  |  |  |  |  |  |  |     \------------------ mode AC:0x29 DC:0x31 Ohm:0x21
|  |  |  |  |  |  |  \------------------------ range 0x32 0~60, 0x34 0~600, 0x31 0~6, 0x30 0~800
|  |  |  |  |  |  \--------------------------- always 0x20 separator ???
|  |  |  |  |  \------------------------------ 4th char value
|  |  |  |  \--------------------------------- 3rd char value
|  |  |  \------------------------------------ 2nd char value
|  |  \--------------------------------------- 1st char value
|  \------------------------------------------ signal char
\--------------------------------------------- always 0xA0 delimitator ???

```
